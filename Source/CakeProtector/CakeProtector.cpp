// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CakeProtector.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, CakeProtector, "CakeProtector" );

DEFINE_LOG_CATEGORY(LogCakeProtector)
 