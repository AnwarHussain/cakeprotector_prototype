// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "CakeProtectorGameMode.h"
#include "CakeProtectorPlayerController.h"
#include "CakeProtectorCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACakeProtectorGameMode::ACakeProtectorGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ACakeProtectorPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/GingerBread_Assets/GingerBreadCharacter1"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}